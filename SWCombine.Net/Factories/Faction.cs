﻿using System.IO;
using System.Net;
using System.Text;

namespace SWCombine.Net.Factories
{
    /// <summary>
    ///     Factory for the Factions.
    /// </summary>
    public sealed class Faction
    {
        /// <summary>
        /// Gets the faction data for the requested faction. This is then returned back into 
        /// the entity that is assigned.
        /// </summary>
        /// <param name="url">Faction API URL. If left blank, it will use the 
        /// /ws/v1.0/faction/ URL by default.</param>
        /// <param name="faction">The faction you are wanting to get data for. Not case sensitive.</param>
        /// <returns>JSON from web request</returns>
        public string GetFactionData(string url, string faction)
        {
            if (url == "")
            {
                url = "http://www.swcombine.com/ws/v1.0/faction/";   
            }
            var request = (HttpWebRequest)WebRequest.Create(string.Concat(url, faction));
            request.Accept = "application/json";

            var response = request.GetResponse();

            using (var responseStream = response.GetResponseStream())
            {
                var reader = new StreamReader(responseStream, Encoding.UTF8);
                return reader.ReadToEnd();
            }
        }
    }
}