﻿using System.Net;

namespace SWCombine.Net.Factories
{
    /// <summary>
    /// Handle Check class holder
    /// </summary>
    public class HandleCheck
    {
        /// <summary>
        /// Takes the hanlde that is requested to check with the combine
        /// database to make sure it is valid.
        /// </summary>
        /// <param name="url">URL for the API call</param>
        /// <param name="handleName">Handle that you want to verify</param>
        /// <returns>
        /// Status code from web reponse.
        /// If the handle is valid it will return a 200 OK
        /// If the handle is invalid it will return a 404 Not Found.
        /// </returns>
        public int CheckHandle(string url, string handleName)
        {
            if (url == "")
            {
                url = "http://www.swcombine.com/ws/v1.0/handle/";   
            }
            var request = (HttpWebRequest)WebRequest.Create(string.Concat(url, handleName));
            request.Accept = "application/json";
            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                return (int)response.StatusCode;
            }
            catch (WebException ex)
            {
                return 404;
            }

        }
    }
}