﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;


namespace SWCombine.Net.Factories
{
    /// <summary>
    /// Factories for the API class.
    /// </summary>
    public class Api
    {
        /// <summary>
        /// Get's the current Galactic Time. 
        /// If no URL is provided the default will be used.
        /// </summary>
        /// <param name="url">URL to obtain time from. May be null</param>
        /// <returns>Galactic time in JSON</returns>
        public string GetGalacticTime(string url)
        {
            
            if (url == "")
            {
                url = "http://www.swcombine.com/ws/v1.0/api/time/cgt/";
            }

            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Accept = "application/json";
            var response = request.GetResponse();
            using (var responseStream = response.GetResponseStream())
            {
                var reader = new StreamReader(responseStream, Encoding.UTF8);
                return reader.ReadToEnd();
            }
        }

        /// <summary>
        /// Checks to see if the requested handle is a valid handle.
        /// If no URL is provided the default will be used.
        /// </summary>
        /// <param name="url">URL to obtain check from. May be null.</param>
        /// <param name="handle">Full handle that is being checked.</param>
        /// <returns>int with status code. 200 for valid handle - 404 for invalid handle.</returns>
        public int GetHandleInfo(string url, string handle)
        {
            if (url == "")
            {
                url = "http://www.swcombine.com/ws/v1.0/handle/";
            }

            var request = (HttpWebRequest)WebRequest.Create(string.Concat(url, handle));
            request.Accept = "application/json";
            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                return (int)response.StatusCode;
            }
            catch (WebException)
            {
                return 404;
            }
        }
    }
}
