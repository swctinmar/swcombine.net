﻿using System.Runtime.Serialization;

namespace SWCombine.Net.Entities.Api
{
    /// <summary>
    /// Creates a Response calls to hold information for
    /// the HelloWorld API call.
    /// </summary>
    [DataContract]
    public class HelloWorldResponse
    {
        /// <summary>
        /// SWC API Version
        /// </summary>
        [DataMember(Name = "version")]
        public string Version { get; set; }

        /// <summary>
        /// Unix Timestamp
        /// </summary>
        [DataMember(Name = "timestamp-swc")]
        public long SWCTimeStamp { get; set; }

        /// <summary>
        /// Class for HelloWorld message.
        /// </summary>
        [DataMember(Name = "helloworld")]
        public HelloWorld HelloWorld { get; set; }
    }

    /// <summary>
    /// HelloWorld class
    /// </summary>
    [DataContract]
    public class HelloWorld
    {
        /// <summary>
        /// Holder for HelloWorld message.
        /// </summary>
        [DataMember(Name = "message")]
        public string Message { get; set; }
    }

}
