﻿using System.Runtime.Serialization;

namespace SWCombine.Net.Entities.Api
{
    /// <summary>
    /// Get's the current Central Galactic Time (CGT)
    /// </summary>
    [DataContract]
    public class SwcTimeResponse
    {
        [DataMember(Name = "version")]
        public string Version { get; set; }

        [DataMember(Name = "timestamp-swc")]
        public long SwcTimeStamp { get; set; }

        [DataMember(Name = "timecgt")]
        public CgtTime CgtTime { get; set; }
    }

    /// <summary>
    /// Breaks down the time into Years, Days, Hours,
    /// Mins and Secs
    /// </summary>
    [DataContract]
    public class CgtTime
    {
        [DataMember(Name = "years")]
        public string Year { get; set; }

        [DataMember(Name = "days")]
        public string Day { get; set; }

        [DataMember(Name = "hours")]
        public string Hour { get; set; }

        [DataMember(Name = "mins")]
        public string Minute { get; set; }

        [DataMember(Name = "secs")]
        public string Second { get; set; }
    }
}