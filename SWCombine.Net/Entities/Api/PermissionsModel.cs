﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SWCombine.Net.Entities.Api
{
    /// <summary>
    /// Shell that holds data returned from the 
    /// Permissions API call.
    /// </summary>
    public class PermissionsResponse
    {
        /// <summary>
        /// API Version
        /// </summary>
        [DataMember(Name = "version")]
        public string Version { get; set; }

        /// <summary>
        /// Unix Timestamp
        /// </summary>
        [DataMember(Name = "timestamp-swc")]
        public long SWCTimeStamp { get; set; }
    }

    /// <summary>
    /// Class for the list of permissions that are
    /// returned
    /// </summary>
    public class PermissionList
    {
        /// <summary>
        /// List of permission that were returned.
        /// </summary>
        public List<Permission> Permission { get; set; }
    }

    /// <summary>
    /// Signluar permission that was returned.
    /// </summary>
    public abstract class Permission
    {
        /// <summary>
        /// Name of permission.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Description of the permission
        /// </summary>
        public string Description { get; set; }
    }

}