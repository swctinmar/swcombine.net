﻿using System.Runtime.Serialization;

namespace SWCombine.Net.Entities.Galaxy
{
    [DataContract]
    public class SystemPlanetsResponse
    {
        /// <summary>
        /// SWC API Version
        /// </summary>
        [DataMember(Name = "version")]
        public string Version { get; set; }

        /// <summary>
        /// Unix Timestamp
        /// </summary>
        [DataMember(Name = "timestamp-swc")]
        public long SWCTimeStamp { get; set; }
        
        
    }
    
    /// <summary>
    /// Holder for the Planets attribute
    /// </summary>
    [DataContract(Name = "galaxy-system-planets")]
    public class GalaxySystemPlanets
    {
        /// <summary>
        /// Galaxy System Planet Attribute
        /// </summary>
        [DataMember(Name = "attributes")]
        public GalaxySystemPlanetsAttributes Attributes { get; set; }
    }

    /// <summary>
    /// Galaxy System Planet Attribute
    /// </summary>
    [DataContract(Name = "attributes")]
    public class GalaxySystemPlanetsAttributes
    {
        /// <summary>
        /// The count of how many planets are in the system
        /// </summary>
        [DataMember(Name = "count")]
        public string Count { get; set; }
    }

    /// <summary>
    /// Planet Attribute holder
    /// </summary>
    [DataContract(Name = "planet")]
    public class Planet
    {
        /// <summary>
        /// Get's the list of planets and stores them.
        /// </summary>
        [DataMember(Name = "attributes")]
        public PlanetAttributes PlanetAttributes { get; set; }
    }

    /// <summary>
    /// Holds planet based information.
    /// </summary>
    [DataContract(Name = "attributes")]
    public class PlanetAttributes
    {
        [DataMember(Name = "uid")]
        public string Uid { get; set; }
        [DataMember(Name = "name")]
        public string Name { get; set; }
        [DataMember(Name = "href")]
        public string Href { get; set; }
    }
}