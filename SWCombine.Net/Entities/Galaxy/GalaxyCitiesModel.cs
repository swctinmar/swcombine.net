﻿using System.Runtime.Serialization;
using System.Xml.Schema;

namespace SWCombine.Net.Entities.Galaxy
{
    [DataContract]
    public class GalaxyCitiesResponse
    {
        /// <summary>
        /// SWC API Version
        /// </summary>
        [DataMember(Name = "version")]
        public string Version { get; set; }

        /// <summary>
        /// Unix Timestamp
        /// </summary>
        [DataMember(Name = "timestamp-swc")]
        public long SWCTimeStamp { get; set; }
        
        
    }

    /// <summary>
    /// Summary documentation coming.
    /// </summary>
    [DataContract(Name = "galaxy-cities")]
    public class GalaxyCities
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "attributes")]
        public GalaxyCitiesAttribute Attribute { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "city")]
        public GalaxyCitiesCity City { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    [DataContract(Name = "attributes")]
    public class GalaxyCitiesAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "start")]
        public string Start { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "total")]
        public string Total { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "count")]
        public string Count { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    [DataContract(Name = "city")]
    public class GalaxyCitiesCity
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "attributes")]
        public GalaxyCitiesAttribute CityAttribute { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    [DataContract(Name = "attributes")]
    public class GalaxyCityAttributes
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "uid")]
        public string Uid { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "name")]
        public string Name { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "Href")]
        public string Href { get; set; }
    }
}