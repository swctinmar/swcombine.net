﻿using System.Runtime.Serialization;

namespace SWCombine.Net.Entities.Faction
{
    /// <summary>
    /// Response for calling the Faction information.
    /// </summary>
    public class FactionResponse
    {
        /// <summary>
        /// SWC API Version
        /// </summary>
        [DataMember(Name = "version")]
        public string Version { get; set; }

        /// <summary>
        /// Unix Timestamp
        /// </summary>
        [DataMember(Name = "timestamp-swc")]
        public long SWCTimeStamp { get; set; }

        /// <summary>
        /// Data from the requested faction.
        /// </summary>
        [DataMember(Name = "faction")]
        public Faction Faction { get; set; }
    }

    /// <summary>
    /// Faction details
    /// </summary>
    [DataContract]
    public class Faction
    {
        /// <summary>
        /// Lists the attributes of the Faction such as
        /// `isBasic`
        /// </summary>
        [DataMember(Name = "attributes")]
        public Attributes Attributes { get; set; }

        /// <summary>
        /// Unique ID of the Faction
        /// </summary>
        [DataMember(Name = "uid")]
        public string Uid { get; set; }

        /// <summary>
        /// Name of the Faction
        /// </summary>
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Faction description in HTML markup
        /// </summary>
        [DataMember(Name = "description")]
        public string Description { get; set; }

        /// <summary>
        /// Category of the Faction
        /// such as 'Government', 'Mining', etc.
        /// </summary>
        [DataMember(Name = "category")]
        public string Category { get; set; }

        /// <summary>
        /// Faction colors
        /// </summary>
        [DataMember(Name = "colour")]
        public Color Color { get; set; }

        /// <summary>
        /// Type of faction, such as Government, Mning, etc.
        /// </summary>
        [DataMember(Name = "type")]
        public Type Type { get; set; }

        /// <summary>
        /// Faction Leader information.
        /// </summary>
        [DataMember(Name = "leader")]
        public Leader Leader { get; set; }

        /// <summary>
        /// Get's the founding date of the faction
        /// </summary>
        [DataMember(Name = "foundation-date")]
        public FoundationDate FoundationDate { get; set; }

        /// <summary>
        /// Get's the IRC channel or Discord channel
        /// </summary>
        [DataMember(Name = "irc-room")]
        public string IRCRoom { get; set; }

        /// <summary>
        /// Get's the factions holosite URL
        /// </summary>
        [DataMember(Name = "homepage")]
        public string Homepage { get; set; }

        //TODO: Impliement the recruitment-liasons

        //TODO: Impliment the data-cards

        /// <summary>
        /// Get's the images for the faction
        /// </summary>
        [DataMember(Name = "images")]
        public Images Images { get; set; }
    }

    /// <summary>
    /// Faction attributes
    /// </summary>
    [DataContract]
    public class Attributes
    {
        /// <summary>
        /// If the faction is basic or not.
        /// </summary>
        [DataMember(Name = "isbasic")]
        public bool IsBasic { get; set; }
    }

    /// <summary>
    /// Faction color
    /// </summary>
    [DataContract]
    public class Color
    {
        /// <summary>
        /// Red level of Factions color
        /// </summary>
        [DataMember(Name = "r")]
        public string R { get; set; }

        /// <summary>
        /// Green level of Faction's color
        /// </summary>
        [DataMember(Name = "g")]
        public string G { get; set; }

        /// <summary>
        /// Blue level of Faction's color
        /// </summary>
        [DataMember(Name = "b")]
        public string B { get; set; }
    }

    /// <summary>
    /// Faction type
    /// </summary>
    [DataContract]
    public class Type
    {
        /// <summary>
        /// Faction type data
        /// </summary>
        [DataMember(Name = "attributes")]
        public TypeAttributes TypeAttributes { get; set; }

        /// <summary>
        /// Type value
        /// </summary>
        [DataMember(Name = "value")]
        public string Value { get; set; }
    }

    /// <summary>
    /// Faction type attributes
    /// </summary>
    [DataContract]
    public class TypeAttributes
    {
        /// <summary>
        /// Unique ID for the type of Faction
        /// </summary>
        [DataMember(Name = "uid")]
        public string Uid { get; set; }

        /// <summary>
        /// API Url for the type
        /// </summary>
        [DataMember(Name = "href")]
        public string Href { get; set; }
    }

    /// <summary>
    /// Faction leader information
    /// </summary>
    [DataContract]
    public class Leader
    {
        /// <summary>
        /// Get's the attributes for the leader of the faction.
        /// </summary>
        [DataMember(Name = "attributes")]
        public LeaderAttributes LeaderAttributes { get; set; }

        /// <summary>
        /// Returns the leader of the factions' name.
        /// </summary>
        [DataMember(Name = "value")]
        public string Value { get; set; }
    }

    /// <summary>
    /// Faction leader attributes
    /// </summary>
    [DataContract]
    public class LeaderAttributes
    {
        /// <summary>
        /// Returns the leader's Unique ID
        /// </summary>
        [DataMember(Name = "uid")]
        public string Uid { get; set; }

        /// <summary>
        /// Returns API link to the leader
        /// </summary>
        [DataMember(Name = "href")]
        public string Href { get; set; }
    }

    /// <summary>
    /// Second in charge information
    /// </summary>
    [DataContract]
    public class SecondIC
    {
        /// <summary>
        /// Get's the attributes for the 2IC of the faction
        /// </summary>
        [DataMember(Name = "uid")]
        public SecondICAttributes SecondICAttributes { get; set; }

        /// <summary>
        /// Get's the 2IC's name
        /// </summary>
        [DataMember(Name = "value")]
        public string Value { get; set; }
    }

    /// <summary>
    /// Gets the 2IC attributes
    /// </summary>
    [DataContract]
    public class SecondICAttributes
    {
        /// <summary>
        /// Returns the 2IC Unique ID
        /// </summary>
        [DataMember(Name = "uid")]
        public string Uid { get; set; }

        /// <summary>
        /// Returns API link to the 2IC
        /// </summary>
        [DataMember(Name = "href")]
        public string Href { get; set; }
    }

    /// <summary>
    /// Get's the date the faction was founded
    /// </summary>
    [DataContract]
    public class FoundationDate
    {
        /// <summary>
        /// Unix Timestamp
        /// </summary>
        [DataMember(Name = "timestamp-swc")]
        public static string SWCTimestamp { get; set; }

        /// <summary>
        /// Combine Galactic Timestamp
        /// </summary>
        [DataMember(Name = "timestamp-cgt")]
        public static string CGTTimestamp { get; set; }
    }

    /// <summary>
    /// Get's the factions images
    /// </summary>
    [DataContract]
    public class Images
    {
        /// <summary>
        /// Factions Logo
        /// </summary>
        [DataMember(Name = "logo")]
        public string Logo { get; set; }

        /// <summary>
        /// Horizontal banner for the faction.
        /// </summary>
        [DataMember(Name = "horizontal-banner")]
        public string HorizontalBanner { get; set; }

        /// <summary>
        /// Vertical banner for the faction
        /// </summary>
        [DataMember(Name = "vertical-banner")]
        public string VerticalBanner { get; set; }
    }
}