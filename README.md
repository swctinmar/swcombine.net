## SWCombine.Net
*A `.net` library to connect to the [Star Wars Combine](http://www.swcombine.com) API.*

[![Build status](https://ci.appveyor.com/api/projects/status/3h3451uyw9wvl100?svg=true)](https://ci.appveyor.com/project/MarciniusTurelles/swcombine-net-i7jec)

As of right now I'm still buidling this out but plan to build it out correctly. If you want to help out, please feel free to place a pull request in.
I may move this project over to Github for visability.

If you write codes, please write tests along with them.
