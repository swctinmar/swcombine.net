﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Newtonsoft.Json;
using SWCombine.Net.Entities.Faction;
using SWCombine.Net.Entities.Api;
using SWCombine.Net.Factories;

namespace SWCombine.Net.Tests
{
    [TestFixture]
    public class SwCombineTests
    {
        [Test]
        public void CheckTestFramework() => Assert.Pass();

        [Test]
        public void SwCombine_Faction_WhenNullUrl_ReturnData()
        {
            // Arrange
            var getFaction = new Factories.Faction();
            var factionData = getFaction.GetFactionData("", "Tresario Star Kingdom");

            // Act
            var data = JsonConvert.DeserializeObject<FactionResponse>(factionData);

            // Assert
            CheckFactionData(data);
        }

        [Test]
        public void SwCombine_Faction_WhenUrl_ReturnData()
        {
            const string url = "http://www.swcombine.com/ws/v1.0/faction/";
            const string faction = "Tresario Star Kingdom";
            var getFaction = new Factories.Faction();

            var factionData = getFaction.GetFactionData(url, faction);
            var data = JsonConvert.DeserializeObject<FactionResponse>(factionData);

            CheckFactionData(data);
        }
        
        [Test]
        public void SwComebine_Time_WhenUrl_ReturnData()
        {
            const string url = "http://www.swcombine.com/ws/v1.0/api/time/cgt/";

            var getTime = new Api();

            var time = getTime.GetGalacticTime(url);

            var data = JsonConvert.DeserializeObject<CgtTime>(time);
        }

        private static void CheckFactionData(FactionResponse data)
        {
            if (data == null)
            {
                Assert.Fail("Faction data returned null.");
            }
            else
            {
                Assert.Pass(data.ToString());
            }
        }
    }
}
